# Manganizer

Scrape a certain website for manga and save the files for offline reading.  

## Usage
```bash
# Downloads episode 42
$./main 42

# Downloads episodes 33 to 70
$./main 33 70
```
Episodes are stored in ```output_files```.

## What does it do?
Honestly, I just wanted to read Bleach on my Windows tablet whose  
internal storage is so insufficient that it's impossible to run a decent  
browser with Windows 10 eating up all the memory. (Also, the CPU does not  
support linux.)

Solution: 

* run a script to download separate manga pages,  
* view manga offline.

## Todo:
After about 80 file transfers, http.Get returns an error "too many open files". I'm assuming the http connections aren't closed properly in spite of calling res.Body.Close().  
In ``testing_socket.test```, we can setabilsh cca 100 connections with a 3s sleep after each get, about 400 connections without sleep.  

## Notes

* ```testing_socket.test```: if you rename it to ```testing_socket.go```, ```../...``` starts complaining about the contents of the package. Rename only when testing.  
* We can work around the open files limit by calling:  
```for i in {1..400}; do ./main $i >> log.txt; done```.  