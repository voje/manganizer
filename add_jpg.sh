#!/bin/bash

# Add .jpg to all files in ./output_files

cd ./output_file
for d in $(ls); do
	cd $d
	for f in $(ls); do
		if [ -f $f ]; then
			mv $f "$f.jpg"
			#echo $(pwd)
		fi
	done	
	cd ..
done
cd ..
