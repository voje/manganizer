package main

import (
	"fmt"
	"net/http"
	"github.com/anaskhan96/soup"
	"io/ioutil"
	"os"
	"strconv"
	//"github.com/ti/nasync"
)

var g_url string
var g_dir string
var finished map[int] chan bool 	//keep track of every episode download
var g_page_block = 16    //guess

func check(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
}

func dl_episode(episode_nr int) {
	fmt.Printf(" -- Downloading episode %d. -- \n", episode_nr)

	//make directory for the episode
	err := os.MkdirAll(fmt.Sprintf("%s/%d", g_dir, episode_nr), 0766)
	check(err)

	//call go routine for every page
	finished[episode_nr] = make(chan bool)
	var page_nr int = 1
	var keep_going bool = true
	for keep_going {
		fmt.Printf("Downloading block: [%d, %d).\n", page_nr, page_nr + g_page_block)
		fcount := 0
		for i:=0; i<g_page_block; i++ {
			go dl_page(fmt.Sprintf("%s/%d/%d", g_url, episode_nr, page_nr), fmt.Sprintf("%s/%d/%d", g_dir, episode_nr, page_nr), episode_nr)
			fcount++
			page_nr++
		}
		//wait for all pages to finish downloading
		for i:=0; i<fcount; i++ {
			//scape blocks of N pages until we hit the first 404
			//fmt.Println(fcount)
			tmp := <-finished[episode_nr]	//necessary for blocking
			keep_going = keep_going && tmp
		}
	}
	fmt.Printf(" -- Finished downloading episode %d. -- \n", episode_nr)
	delete(finished, episode_nr)
}

func dl_page(a_url string, a_path string, episode_nr int) {
	//read page
	res,err := http.Get(a_url)

	//check for 404
	check(err)
	if res.StatusCode == 404 {
		fmt.Printf("%s\t404\n", a_url)
		finished[episode_nr] <- false
		return
	}

	//page to string
	body,err := ioutil.ReadAll(res.Body) 
	check(err)
	res.Body.Close()

	//parse page
	dom := soup.HTMLParse(string(body))

	//find elements
	div_hi := dom.Find("div", "id", "imgholder")
	div_lo := div_hi.Find("img", "id", "img")
	img_src := div_lo.Attrs()["src"]

	//fmt.Println(img_src)
	img,err := soup.Get(img_src)
	check(err)

	//save img as jpg
	err = ioutil.WriteFile(fmt.Sprintf("%s.jpg", a_path), []byte(img), 0644)
	check(err)

	fmt.Printf("%s\tOK\n", a_url)
	finished[episode_nr] <- true
}

func main() {
	fmt.Println("Starting Manganizer.")

	g_url = "http://www.mangareader.net/bleach"
	fmt.Println("Scraping website: " + g_url + ".")

	//make output_file directory
	g_dir = "./output_file"
	err := os.MkdirAll(fmt.Sprintf("%s", g_dir), 0766)
	check(err)

	//init channels map
	finished = make(map[int] chan bool)	

	//read arguments
	var episode_nr1 int = -1
	var episode_nr2 int = -1
	if len(os.Args) == 2 {
		i64,err := strconv.ParseInt(os.Args[1], 10, 64)
		check(err)
		episode_nr1 = int(i64)
		fmt.Printf("Scraping episode %d.\n", episode_nr1)
	} else if len(os.Args) == 3 {
		i64,err := strconv.ParseInt(os.Args[1], 10, 64)
		check(err)
		episode_nr1 = int(i64)
		i64,err = strconv.ParseInt(os.Args[2], 10, 64)
		check(err)
		episode_nr2 = int(i64)
		if episode_nr1 < episode_nr2 {
			fmt.Printf("Scraping from episode %d to episode %d.\n", episode_nr1, episode_nr2)
		} else {
			fmt.Println("First argument should be lower than seconds one.")
			return
		}
	} else {
		fmt.Printf("Insert episode number. Two arguments will download everything from first to second episode number.\n")	
		return
	}

	if episode_nr2 == -1 {
		dl_episode(episode_nr1)
	} else {
		for i:=episode_nr1; i<=episode_nr2; i++ {
			dl_episode(i)
		}		
	}

	fmt.Printf("SUCCESS!\nCheck %s.\n", g_dir)
}
